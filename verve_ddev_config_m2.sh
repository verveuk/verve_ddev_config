#!/bin/bash

############## FUNCTIONS ####################
function set_dev_defaults {
    # set developer mode
    #ddev magento deploy:mode:set developer

    # Disable 2FA
    #ddev magento module:disable Magento_TwoFactorAuth 

    # Set dev admin route
    #ddev magento setup:config:set --backend-frontname="site_admin" --no-interaction
	sed -i "/frontName/c\                'frontName'=>'site_admin'" "app/etc/env.php"

    # set admin session
    ddev magento config:set admin/security/session_lifetime 31536000

    # Add a temp admin user
    ddev magento admin:user:create --admin-user=verveadmin --admin-password='admin123' --admin-email=mark.verve-admin@verve-design.co.uk --admin-firstname=Verve-Admin --admin-lastname=Verve-Admin
	
	# Disable Csp
	#ddev magento module:disable Magento_Csp
	
	# set up cookies
	ddev magento config:set web/cookie/cookie_path /
	ddev magento config:set web/cookie/cookie_domain .ddev.site
	
	# disable Mageplaza SMTP email: (don't disable module as it will get disabled in config.php (which is in git) - just disable the sending)
	ddev magento config:set smtp/developer/developer_mode 1
	
	# housekeeping
	rm -rf pub/static/* && rm -rf var/cache/* && rm -rf var/page_cache/* && rm -rf var/view_preprocessed/* && rm -rf generated/*
}

function install_sample_data {
	ddev magento sampledata:deploy
	ddev magento module:enable --all
	ddev magento setup:upgrade
}

function install_elasticsearch {
	ddev get ddev/ddev-elasticsearch
}

function install_latest_magento {
	DIRECTORY_NAME=${PWD##*/}
	DDEV_ENDING=".ddev.site"
	ddev start
	ddev composer create --repository=https://repo.magento.com/ magento/project-community-edition=$MAG_VERSION

	rm -rf app/etc/env.php
	
	#if [ "$DOMAIN" == "" ]; then
	#	URL="$DIRECTORY_NAME$DDEV_ENDING"
	#else
	#	URL="$DOMAIN"
	#	sed -i "/use_dns_when_possible: true/c\use_dns_when_possible: false" "$DDEV_CONFIG"
	#	sed -i "/additional_fqdns: \[\]/c\additional_fqdns: [$URL]" "$DDEV_CONFIG"
	#fi	
	
	if [[ $MAG_VERSION == *"2.3"* ]]; then
      ddev magento setup:install --base-url=https://$URL/ --cleanup-database --db-host=db --db-name=db --db-user=db --db-password=db --admin-firstname=Magento --admin-lastname=User --admin-email=mark@verve-design.co.uk --admin-user=verve --admin-password=admin123
	else
	  ddev magento setup:install --base-url=https://$URL/ --cleanup-database --db-host=db --db-name=db --db-user=db --db-password=db --elasticsearch-host=elasticsearch --admin-firstname=Magento --admin-lastname=User --admin-email=mark@verve-design.co.uk --admin-user=verve --admin-password=admin123
	  ddev magento module:disable Magento_TwoFactorAuth
    fi
	
    # setup dev defaults
    set_dev_defaults
	ddev magento cache:disable full_page
}

function new_install {
	ddev config --project-type=magento2 --docroot=pub --composer-version=$COMPOSER_VERSION --create-docroot --php-version=$PHP_VERSION
	
	install_elasticsearch
	install_latest_magento
	read -rep $'**************************************\nDo you want to install sample data? [y/n]:' REPLY
	echo    # (optional) move to a new line
	if [ "$REPLY" = "y" ]; then
		install_sample_data
	fi
}

function existing_install {

	DIRECTORY_NAME=${PWD##*/}
	DDEV_ENDING=".ddev.site"
	ENV_FILENAME="app/etc/env.php"

	# next check if Magento 2 files are present:
	MAG_FILENAME="app/bootstrap.php"
	if [ ! -f "$MAG_FILENAME" ]; then
		echo "[ERROR] $MAG_FILENAME does not exist. Please ensure Magento 2 files are downloaded."
		exit
	fi

	chmod u+x bin/magento

	# Start ddev
	echo "Starting ddev config..."
	

	if [ "$DOMAIN" == "" ]; then
		URL="$DIRECTORY_NAME$DDEV_ENDING"
		PROJECT_NAME="$DIRECTORY_NAME"
	else
		URL="$DOMAIN$DDEV_ENDING"
		PROJECT_NAME="$DOMAIN"
		#sed -i "/use_dns_when_possible: true/c\use_dns_when_possible: false" "$DDEV_CONFIG"
		#sed -i "/additional_fqdns: \[\]/c\additional_fqdns: [$URL]" "$DDEV_CONFIG"
	fi
	ddev config --php-version=$PHP_VERSION --composer-version=$COMPOSER_VERSION --project-type=magento2 --project-name=$PROJECT_NAME
	# first check if elasticsearch is installed:
	install_elasticsearch

	# update env.php with db credentials
	echo "Updating database credentials in env.php..."
	sleep 2
	sed -i "/host/c\                'host'=>'db'," "$ENV_FILENAME"
	sed -i "/username/c\                'username'=>'db'," "$ENV_FILENAME"
	sed -i "/password/c\                'password'=>'db'," "$ENV_FILENAME"
	sed -i "/dbname/c\                'dbname'=>'db'," "$ENV_FILENAME"

	echo "Starting ddev..."
	ddev start

	# check for vendor files
	VENDOR_FILENAME="vendor/autoload.php"
	if [ ! -f "$VENDOR_FILENAME" ]; then
		echo "$VENDOR_FILENAME does not exist. Running composer install. (auth keys may be required)"
		yes | ddev composer install
	fi


	# import database
	echo "Importing database from file "$DB_FILE" (this could take a while - grab a coffee)"
	ddev import-db --file=$DB_FILE


	# set Magento 2 to use elasticsearch ddev host
    if [[ $MAG_VERSION == *"2.4"* ]]; then	
      echo "Setting elasticsearch host..."
	  ddev magento config:set catalog/search/elasticsearch7_server_hostname elasticsearch
	fi


	# update base urls
	echo 'Setting base urls to https://'$URL'/...'
	ddev magento setup:store-config:set --base-url-secure="https://"$URL"/"
	ddev magento setup:store-config:set --base-url="https://"$URL"/"

	# clean Magento 2
	echo "Housekeeping..."
	rm -rf pub/static/frontend/* && rm -rf var/cache/* && rm -rf var/page_cache/* && rm -rf var/view_preprocessed/* rm -rf generated/*
	sleep 1

    # setup dev defaults
    set_dev_defaults

	ddev magento cache:disable full_page block_html

	echo "Reindexing..."
	ddev magento indexer:reset
	ddev magento indexer:reindex

	echo "All done and ready for you... Launching ddev describe"
	sleep 2
	ddev describe
}
############## [END] FUNCTIONS ####################



################ MAIN CODE #######################
# GLOBAL VARS
DDEV_CONFIG=".ddev/config.yaml"

# Go
read -rep $'Enter domain name (or blank to use ddev default) :' DOMAIN
read -rep $'\nMagento version required (2.3.3, 2.4.4 etc)?: ' MAG_VERSION
read -rep $'\nPHP version required (7.3, 7.4, 8.1 etc)?: ' PHP_VERSION
read -rep $'\nComposer version required (1, 2)?: ' COMPOSER_VERSION
read -rep $'\nPlease enter the DB import file name to import: ' DB_FILE


existing_install
ddev restart
echo "DONE! :-) Admin login details are: user=verveadmin   pass=admin123"
ddev launch
